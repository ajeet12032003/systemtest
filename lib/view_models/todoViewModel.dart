import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../Model/todo_model.dart';
import '../Router/navigateRouter.dart';

class TodoViewModel extends ChangeNotifier {
  final _auth = FirebaseFirestore.instance;
  // final userauth = FirebaseAuth.instance;
  // User? user = userauth.currentUser;

  //  final _auth = FirebaseAuth.instance;
  List<Todo_model> todosList = [];
  var login_usrname;

  List<Todo_model> get todos => todosList;
  bool isloading = true;
  void addTodo(Todo_model todo) {
    todosList.add(todo);
    notifyListeners();
  }

  Stream<List<Todo_model>> GetTodoDetails() async* {
    final auth1 = FirebaseAuth.instance;
    User? user = auth1.currentUser;
    login_usrname = auth1.currentUser!.email;
    final response = await _auth
        .collection('tasks')
        .doc(user!.uid)
        .collection('mytasks')
        .where('uid', isEqualTo: user.uid)
        .get();
// addTodo
    final response1 = await _auth.collection('issharewith').where(
        'issharewidth',
        arrayContainsAny: [auth1.currentUser!.email]).get();
    print("object");
    print(response1.docs);

    List<DocumentSnapshot> mergedResults = [
      ...response.docs,
      ...response1.docs
    ];

    final userdata = mergedResults.map((e) => Todo_model.fromMap(e)).toList();
    todosList = userdata;
    print("Mt todo data");
    print(todosList);
    isloading = false;
    notifyListeners();
    // return userdata;
  }

  logout(BuildContext context) async {
    await FirebaseAuth.instance.signOut();
    notifyListeners();
    NavigatePage().NavigateToSplashScreen(context);
  }

  updateTodoDetailsById(DateTime datetime, title) async {
    final auth1 = FirebaseAuth.instance;
    User? user = auth1.currentUser;
    print(datetime);
    final response = await _auth
        .collection('tasks')
        .doc(user!.uid)
        .collection('mytasks')
        .where('dateTime', isEqualTo: '$datetime')
        .get()
        .then((QuerySnapshot snapshot) {
      //Here we get the document reference and return to the post variable.
      return snapshot.docs[0].reference;
    });
    print("AFTER");
    // print(response.docs);
    var batch = _auth.batch();
    //Updates the field value, using post as document reference
    batch.update(response, {'description': title});
    batch.commit();

    //update in second table

    final response1 = await _auth
        .collection('issharewith')
        .where('dateTime', isEqualTo: '$datetime')
        .get()
        .then((QuerySnapshot snapshot) {
      //Here we get the document reference and return to the post variable.
      return snapshot.docs[0].reference;
    });
    print("seconf update");

    var batch1 = _auth.batch();
    //Updates the field value, using post as document reference
    batch1.update(response1, {'description': title});
    batch1.commit();

    notifyListeners();
  }
}
