import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:provider/provider.dart';
import 'package:systemtest/Model/user_model.dart';

import '../Model/todo_model.dart';
import '../Router/navigateRouter.dart';
import 'todoViewModel.dart';

class todoAdd {
  final _auth = FirebaseAuth.instance;
  StoreAllDetailsToFireStore(
      Todo_model todoModel, BuildContext context, dateTime) async {
    // FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
    // User? user = _auth.currentUser;
    // print(todoModel);
    // await firebaseFirestore
    //     .collection('addTodo')
    //     .add(todoModel.tomap())
    //     // .doc(user!.uid)
    //     //  .set(todoModel.tomap())
    //     .then((value) {
    //   ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
    //       backgroundColor: Colors.red,
    //       content: Text("User Registration SuccessFully Completed")));

    //   // Provider.of<TodoViewModel>(context, listen: false).GetTodoDetails();
    //   NavigatePage().NavigateToTodoScreen(context);
    //   print('ue');
    // });
    print("Method call");
    FirebaseAuth auth = FirebaseAuth.instance;
    FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
    User? user = _auth.currentUser;
    String uid = user!.uid;
    var time = DateTime.now();
    await firebaseFirestore
        .collection('tasks')
        .doc(uid)
        .collection('mytasks')
        .doc(dateTime.toString())
        .set(todoModel.tomap())
        .then((value) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          backgroundColor: Colors.red,
          content: Text("Task Added SuccessFully Completed")));

      Provider.of<TodoViewModel>(context, listen: false).GetTodoDetails();
      NavigatePage().PopToTodoScreen(context);

      print('ue');
    });
  }

  Future getAllRegisteredEmail() async {
    final auth = FirebaseFirestore.instance;
    final response = await auth.collection('users').get();
    print("object");
    print(response);
    final userdata =
        response.docs.map((e) => usermodelDetails.fromMap(e)).toList();
    print("After reso");
    return userdata;
  }

  Future ShareTodoToAnotherUSer(sharevalue, shareBy, BuildContext context,
      datetime, title, description) async {
    FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
    User? user = _auth.currentUser;
    print("User Id");
    print(datetime);

    await firebaseFirestore
        .collection('tasks')
        .doc(user!.uid)
        .collection('mytasks')
        .doc(datetime)
        .update({
      'issharewidth': FieldValue.arrayUnion([sharevalue]),
      'shareby': shareBy
    })

        // .set(todoModel.tomap())
        .then((value) {
      firebaseFirestore.collection('issharewith').doc(datetime).set({
        'issharewidth': FieldValue.arrayUnion([sharevalue]),
        'shareby': shareBy,
        'title': title,
        'description': description,
        'uid': user.uid,
        'dateTime': datetime
      });

      NavigatePage().NavigateToTodoScreen(context);
    });
  }
}
