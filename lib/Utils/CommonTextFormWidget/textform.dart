import 'package:flutter/material.dart';
import 'package:form_validator/form_validator.dart';
import 'package:get/get.dart';

class CommonTextFormWidget extends StatelessWidget {
  late String label;
  late TextEditingController textEditingControllername;
  late IconData iconname;
  TextInputType inputtype;

  CommonTextFormWidget(
      {super.key,
      required this.label,
      required this.textEditingControllername,
      required this.iconname,
      required this.inputtype});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: TextFormField(
        keyboardType: inputtype,
        validator: (value) {
          if (value!.isEmpty) {
            return 'required field';
          }
          return null;
        },
        controller: textEditingControllername,
        decoration: InputDecoration(
            enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white)),
            focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey)),
            fillColor: const Color.fromARGB(255, 207, 206, 206),
            hintText: label,
            prefixIcon: Icon(iconname)
            // prefix: const Icon(Icons.email),
            ),
      ),
    );
  }
}

class CommonTextEmailFormWidget extends StatelessWidget {
  late String label;
  late TextEditingController textEditingControllername;
  late IconData iconname;
  TextInputType inputtype;

  CommonTextEmailFormWidget(
      {super.key,
      required this.label,
      required this.textEditingControllername,
      required this.iconname,
      required this.inputtype});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: TextFormField(
        keyboardType: inputtype,
        validator: ValidationBuilder().email().maxLength(50).build(),
        controller: textEditingControllername,
        decoration: InputDecoration(
            enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white)),
            focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey)),
            fillColor: const Color.fromARGB(255, 207, 206, 206),
            hintText: label,
            prefixIcon: Icon(iconname)
            // prefix: const Icon(Icons.email),
            ),
      ),
    );
  }
}
