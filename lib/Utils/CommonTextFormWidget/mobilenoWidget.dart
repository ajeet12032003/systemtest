import 'package:flutter/material.dart';

class MobileNumber extends StatelessWidget {
  late String label;
  late TextEditingController textEditingControllername;
  late IconData iconname;
  TextInputType inputtype;

  MobileNumber(
      {super.key,
      required this.label,
      required this.textEditingControllername,
      required this.iconname,
      required this.inputtype});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: TextFormField(
        // maxLength: 10,
        keyboardType: inputtype,
        validator: (value) {
          if (value!.isEmpty) {
            return 'required field';
          }
          return null;
        },
        onChanged: (value) {
          if (value.length == 10) {
            FocusScope.of(context).unfocus();
          }
        },
        controller: textEditingControllername,
        decoration: InputDecoration(
          enabledBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white)),
          focusedBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey)),
          fillColor: const Color.fromARGB(255, 207, 206, 206),
          hintText: label,
          prefixIcon: const Icon(Icons.mobile_friendly),
        ),
      ),
    );
  }
}
