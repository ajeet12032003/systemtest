import 'package:flutter/material.dart';

import 'package:systemtest/views/Login/Loginpage.dart';
import 'package:systemtest/views/Splash/SplashPage.dart';

import '../views/Signup/SignUpPage.dart';
import '../views/TODO`S/taskShare.dart';
import '../views/TODO`S/todo_Addtodo.dart';
import '../views/TODO`S/todo_Detailspage.dart';

class NavigatePage {
  NavigateToSignUp(BuildContext context) {
    Navigator.push(context,
        MaterialPageRoute(builder: ((context) => const SignUpScreen())));
  }

  NavigateToLogin(BuildContext context) {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => const LoginScreen()),
        (route) => false);
  }

  NavigateToSplashScreen(BuildContext context) {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => const SplashScreen()),
        (route) => false);
  }

  NavigateToTodoAddPage(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const AddTodoDetails()),
    );
  }

  NavigateToTodoScreen(BuildContext context) {
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => const TodoScreen()),
      (route) => false,
    );
  }

  PopToTodoScreen(BuildContext context) {
    Navigator.pop(
      context,
      MaterialPageRoute(builder: (context) => const TodoScreen()),
    );
  }

  NavigateToTaskShareScreen(
      BuildContext context, datetime1, title, description) {
    print(datetime1);
    print(title);
    print(description);
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => Todo_TaskShare(
              dateTime: datetime1, title: title, description: description)),
    );
  }
}
