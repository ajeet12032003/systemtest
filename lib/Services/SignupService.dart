import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:systemtest/Model/user_model.dart';

import '../Router/navigateRouter.dart';

class SignUpClass {
  final _auth = FirebaseAuth.instance;
  var responsedetails;
  Future SignuptoFirebase(email, password, BuildContext context) async {
    Loader.show(context,
        progressIndicator:
            const Center(child: Center(child: CircularProgressIndicator())));
    try {
      final response = await _auth
          .createUserWithEmailAndPassword(email: email, password: password)
          .then((value) {
        // print(value);
        Loader.hide();
        responsedetails = value;
        print("Inside Succes");
      });
      return responsedetails;
    } catch (e) {
      Loader.hide();
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          backgroundColor: Colors.red,
          content: Text(
              "This Email Already Registered, Please Enter Another Email ")));
    }
  }

  StoreAllDetailsToFireStore(
      usermodelDetails usermodel, BuildContext context) async {
    FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
    User? user = _auth.currentUser;

    await firebaseFirestore
        .collection('users')
        .doc(user!.uid)
        .set(usermodel.tomap())
        .then((value) {
      NavigatePage().NavigateToLogin(context);

      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          backgroundColor: Colors.red,
          content: Text("User Registration SuccessFully Completed")));
      print('ue');
    });
  }
}
