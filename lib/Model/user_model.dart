class usermodelDetails {
  String? uid;
  String? firstname;
  String? lastname;
  String? email;
  String? password;
  String? DOB;
  String? mobileno;

  usermodelDetails(
      {this.uid,
      this.firstname,
      this.lastname,
      this.email,
      this.password,
      this.DOB,
      this.mobileno});

  // Data from server
  factory usermodelDetails.fromMap(map) {
    return usermodelDetails(
      uid: map['uid'],
      firstname: map['firstname'],
      lastname: map['lastname'],
      email: map['email'],
      password: map['password'],
      DOB: map['DOB'],
      mobileno: map['mobileno'],
    );
  }

  // sending data to oserver
  Map<String, dynamic> tomap() {
    return {
      'uid': uid,
      'firstname': firstname,
      'lastname': lastname,
      'email': email,
      'password': password,
      'DOB': DOB,
      'mobileno': mobileno
    };
  }
}
