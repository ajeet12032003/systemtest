import 'package:cloud_firestore/cloud_firestore.dart';

class Todo_model {
  String? uid;
  String? title;
  String? description;
  bool? isCompleted;
  List? issharewidth;
  String? shareby;

  DateTime? dateTime;
  // bool? createdme;

  Todo_model({
    this.uid,
    this.title,
    this.description,
    this.isCompleted,
    this.issharewidth,
    this.shareby,
    this.dateTime,
    // this.createdme
  });

  // sending data to server
  Map<String, dynamic> tomap() {
    return {
      'uid': uid,
      'title': title,
      'description': description,
      'isCompleted': true,
      'issharewidth': [],
      'shareby': shareby,
      'dateTime': dateTime.toString(),
      // 'createdme': true,
    };
  }

//data from server
  factory Todo_model.fromMap(map) {
    return Todo_model(
      uid: map['uid'],
      title: map['title'],
      description: map['description'],
      issharewidth: map.data()['issharewidth'],
      shareby: map['shareby'],
      dateTime: DateTime.parse(map["dateTime"]),
      // createdme: map['createdme'],
    );
  }
}
