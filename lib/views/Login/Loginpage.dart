import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:systemtest/Router/navigateRouter.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:email_auth/email_auth.dart';
import 'package:email_auth/email_auth.dart';

import 'package:flutter/foundation.dart';
import 'package:form_validator/form_validator.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _auth = FirebaseAuth.instance;
  final emailcontoller = TextEditingController();
  final passwordcontroller = TextEditingController();
  bool emailverifiedstatus = false;

  late EmailAuth emailAuth;

  // EmailAuthController emailAuthController = Get.put(EmailAuthController());
  @override
  void initState() {
    super.initState();
    // Initialize the package
    emailAuth = EmailAuth(
      sessionName: "Sample session",
    );

    /// Configuring the remote server
    // emailAuth.config(data)
  }

  final _formkey = GlobalKey<FormState>();
  bool showpassword = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: Container(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Form(
            key: _formkey,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const SizedBox(
                    height: 100,
                  ),
                  SizedBox(
                    width: 150,
                    child: Image.asset("assets/rapidd.png"),
                  ),
                  // Center(
                  //   child: Container(
                  //       child: const Icon(
                  //     Icons.login_sharp,
                  //     size: 50.0,
                  //   )),
                  // ),
                  const Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Sign in to Your Account"),
                      Text(
                        "Welcome To Rapidd Pvt. Ltd ",
                        style: TextStyle(
                            color: Colors.grey, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 50,
                  ),
                  Container(
                    decoration: const BoxDecoration(
                        // color: Color.fromARGB(255, 209, 227, 233)
                        ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10)),
                            child: TextFormField(
                              keyboardType: TextInputType.emailAddress,
                              validator: ValidationBuilder()
                                  .email()
                                  .maxLength(50)
                                  .build(),
                              controller: emailcontoller,
                              decoration: const InputDecoration(
                                enabledBorder: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.white)),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.grey)),
                                fillColor: Color.fromARGB(255, 207, 206, 206),
                                labelText: 'Enter Email',
                                prefixIcon: Icon(Icons.email),
                                // suffixIcon: IconButton(
                                //     onPressed: () {
                                //       // checkEmailRegisterdStatus(

                                //       //     emailcontoller.text);

                                //       sendOtp();

                                //       emailAuthController
                                //           .sendOtp(emailcontoller.value.text);
                                //     },
                                //     icon: const Icon(
                                //       Icons.send,
                                //       color: Colors.blue,
                                //     ))
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Container(
                              color: Colors.white,
                              child: TextFormField(
                                // keyboardType: TextInputType.phone,
                                validator: ValidationBuilder()
                                    .minLength(6)
                                    .maxLength(20)
                                    .build(),
                                obscureText: !showpassword,
                                controller: passwordcontroller,
                                decoration: InputDecoration(
                                    enabledBorder: const OutlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.white)),
                                    focusedBorder: const OutlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.grey)),
                                    fillColor: const Color.fromARGB(
                                        255, 207, 206, 206),
                                    labelText: 'Enter Password',
                                    prefixIcon: const Icon(Icons.password),
                                    suffixIcon: IconButton(
                                        onPressed: () {
                                          setState(() {
                                            if (showpassword) {
                                              showpassword = false;
                                            } else {
                                              showpassword = true;
                                            }
                                          });
                                        },
                                        icon: Icon(showpassword
                                            ? Icons.visibility
                                            : Icons.visibility_off))),
                                // prefix: Icon(Icons.password)),
                              )),
                        ],
                      ),
                    ),
                  ),
                  const Align(
                    alignment: Alignment.centerRight,
                    child: Text("Resend OTP"),
                  ),
                  const SizedBox(
                    height: 60,
                  ),
                  GestureDetector(
                    onTap: () {
                      if (_formkey.currentState!.validate()) {
                        checkEmailRegisterdStatus(emailcontoller.text);
                      }
                    },
                    child: Container(
                        width: MediaQuery.of(context).size.width,
                        decoration: const BoxDecoration(color: Colors.black),
                        child: const Padding(
                          padding: EdgeInsets.all(15.0),
                          child: Center(
                            child: Text(
                              "Sign in",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        )),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
      bottomNavigationBar: SizedBox(
        height: 40,
        child: Center(
            child: GestureDetector(
                onTap: () {
                  NavigatePage().NavigateToSignUp(context);
                },
                child: const Text("Don`t have an account? SignUp"))),
      ),
    );
  }

  void sendOtp() async {
    bool result = await emailAuth.sendOtp(
        recipientMail: emailcontoller.value.text, otpLength: 5);
    if (result) {
      setState(() {
        print("Send OTP");
      });
    }
  }

  Future<void> callToLogin(email, password) async {
    Loader.show(context,
        progressIndicator:
            const Center(child: CircularProgressIndicator.adaptive()));
    try {
      await _auth
          .signInWithEmailAndPassword(email: email, password: password)
          .then((value) {
        print("Inside Login");
        print(value);
        setState(() {
          Loader.hide();
          NavigatePage().NavigateToTodoScreen(context);
        });
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            backgroundColor: Colors.blue, content: Text("Login Successfully")));
      });
    } on FirebaseAuthException catch (e) {
      print('Failed with error code: ${e.code}');
      print(e.message);
      setState(() {
        Loader.hide();
      });
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          backgroundColor: Colors.red,
          content: Text("Login Failed ; ${e.code}")));
    }
  }

  Future<void> checkEmailRegisterdStatus(email) async {
    QuerySnapshot<Map<String, dynamic>> querySnapshot = await FirebaseFirestore
        .instance
        .collection('users')
        .where('email', isEqualTo: email)
        .get();

    print(querySnapshot.docs.length);
    if (querySnapshot.docs.isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          backgroundColor: Colors.red,
          content: Text("This Email not Registered, Please Signup First ")));
    } else if (querySnapshot.docs.isNotEmpty) {
      callToLogin(emailcontoller.text, passwordcontroller.text);
    }
  }
}
