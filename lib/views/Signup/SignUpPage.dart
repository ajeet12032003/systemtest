import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../../Model/user_model.dart';
import '../../Services/SignupService.dart';
import '../../Utils/CommonTextFormWidget/mobilenoWidget.dart';
import '../../Utils/CommonTextFormWidget/textform.dart';
import 'package:intl/intl.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({super.key});

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  TextEditingController firstname = TextEditingController();
  TextEditingController lastname = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController mobileno = TextEditingController();
  TextEditingController dob = TextEditingController();

  TextEditingController password = TextEditingController();
  TextEditingController cnfrmpassword = TextEditingController();

  bool showpassword = false;

  final _auth = FirebaseAuth.instance;
  final _formkey = GlobalKey<FormState>();
  final service = SignUpClass();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // backgroundColor: Colors.red,
        title: const Text("Sign Up Form"),
        centerTitle: true,
      ),
      body: Container(
        decoration: BoxDecoration(color: Colors.grey[300]),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Form(
            key: _formkey,
            child: SingleChildScrollView(
              child: Column(children: [
                const SizedBox(
                  height: 50,
                ),
                const Icon(
                  Icons.lock,
                  size: 40,
                ),
                const SizedBox(
                  height: 10,
                ),
                const Text(
                  "let`s create an account for YOU",
                  style: TextStyle(
                      color: Colors.grey, fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 50,
                ),
                CommonTextFormWidget(
                  label: 'First Name',
                  textEditingControllername: firstname,
                  iconname: Icons.person,
                  inputtype: TextInputType.text,
                ),
                const SizedBox(
                  height: 5,
                ),
                CommonTextFormWidget(
                  label: 'Last Name',
                  textEditingControllername: lastname,
                  iconname: Icons.person,
                  inputtype: TextInputType.text,
                ),
                const SizedBox(
                  height: 5,
                ),
                const SizedBox(
                  height: 5,
                ),
                MobileNumber(
                  label: 'Mobile No',
                  textEditingControllername: mobileno,
                  iconname: Icons.mobile_friendly,
                  inputtype: TextInputType.number,
                ),
                const SizedBox(
                  height: 5,
                ),
                CommonTextEmailFormWidget(
                  label: 'Email',
                  textEditingControllername: email,
                  iconname: Icons.email,
                  inputtype: TextInputType.emailAddress,
                ),
                const SizedBox(
                  height: 5,
                ),
                Container(
                  color: Colors.white,
                  child: TextFormField(
                    readOnly: true,
                    controller: dob,
                    validator: (Value) {
                      if (Value!.isEmpty) {
                        return 'Please Select DOB';
                      }
                      return null;
                    },
                    decoration: const InputDecoration(
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white)),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey)),
                      fillColor: Color.fromARGB(255, 207, 206, 206),
                      hintText: 'Select DOB',
                      prefixIcon: Icon(Icons.date_range),
                    ),
                    onTap: () async {
                      DateTime? pickeddate = await showDatePicker(
                          context: context,
                          initialDate: DateTime(1990),
                          firstDate: DateTime(1990),
                          lastDate: DateTime.now());
                      if (pickeddate != null) {
                        setState(() {
                          dob.text =
                              DateFormat('dd-MM-yyyy').format(pickeddate);
                        });
                      }
                    },
                  ),
                ),
                const SizedBox(
                  height: 6,
                ),
                Container(
                  color: Colors.white,
                  child: TextFormField(
                    obscureText: !showpassword,
                    controller: password,
                    validator: (Value) {
                      if (Value!.isEmpty || Value.length <= 8) {
                        return 'Required Field Or Please Enter Alteast 8 digit';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                        enabledBorder: const OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white)),
                        focusedBorder: const OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey)),
                        fillColor: const Color.fromARGB(255, 207, 206, 206),
                        hintText: 'Set Password',
                        prefixIcon: const Icon(Icons.password),
                        suffixIcon: IconButton(
                            onPressed: () {
                              setState(() {
                                if (showpassword) {
                                  showpassword = false;
                                } else {
                                  showpassword = true;
                                }
                              });
                            },
                            icon: Icon(showpassword
                                ? Icons.visibility
                                : Icons.visibility_off))),
                  ),
                ),
                const SizedBox(
                  height: 6,
                ),
                Container(
                  color: Colors.white,
                  child: TextFormField(
                    validator: (Value) {
                      if (Value!.isEmpty || Value.length <= 8) {
                        return 'Required Field Or Please Enter Alteast 8 digit';
                      } else if (password.text != cnfrmpassword.text) {
                        return 'Password & ConformedPassword not match, please enter Again';
                      }
                      return null;
                    },
                    obscureText: true,
                    controller: cnfrmpassword,
                    decoration: const InputDecoration(
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white)),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey)),
                      fillColor: Color.fromARGB(255, 207, 206, 206),
                      hintText: 'Confirm Password',
                      prefixIcon: Icon(Icons.password),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 60,
                ),
                GestureDetector(
                  onTap: () {
                    if (_formkey.currentState!.validate()) {
                      SignUp();
                    }
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.circular(5)),
                    child: const Center(
                        child: Padding(
                      padding: EdgeInsets.all(15.0),
                      child: Text(
                        "Submit",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                    )),
                  ),
                ),
              ]),
            ),
          ),
        ),
      ),
    );
  }

  void SignUp() async {
    QuerySnapshot<Map<String, dynamic>> querySnapshot = await FirebaseFirestore
        .instance
        .collection('users')
        .where('mobileno', isEqualTo: mobileno.text)
        .get();

    print(querySnapshot.docs.length);

    if (querySnapshot.docs.isNotEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          backgroundColor: Colors.red,
          content: Text(
              "This Mobile No Already Registered, Please Enter Another Number")));
    } else if (querySnapshot.docs.isEmpty) {
      service.SignuptoFirebase(email.text, password.text, context)
          .then((value) {
        print("My data");
        print(value);

        if (value != null) {
          StoreAllDetails();
        }
      });
    } else {}
  }

  StoreAllDetails() async {
    FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
    User? user = _auth.currentUser;

    usermodelDetails usermodel = usermodelDetails();

    usermodel.uid = user!.uid;
    usermodel.firstname = firstname.text;
    usermodel.lastname = lastname.text;
    usermodel.email = user.email;
    usermodel.mobileno = mobileno.text;
    usermodel.DOB = dob.text;
    usermodel.password = password.text;

    service.StoreAllDetailsToFireStore(usermodel, context);
  }
}
