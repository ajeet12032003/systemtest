import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../../Router/navigateRouter.dart';
import '../Login/Loginpage.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    NavigatetoLoginScreen();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // Image.network(
            //     'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS1yYJG3ZkWvaU1UA-t9wW8ucn_E_N1IMWAnqAek6pp1_9e3LlnOsKQu24CfQjehx1kclA&usqp=CAU'),

            Image.asset('assets/rapidd.png'),
            const SizedBox(
              height: 20,
            ),
            const Text("Welcome to My App")
          ],
        ),
      ),
    );
  }

  NavigatetoLoginScreen() {
    Future.delayed(const Duration(seconds: 5), () {
      // code to be executed after 2 seconds
      FirebaseAuth.instance.currentUser != null
          ? NavigatePage().NavigateToTodoScreen(context)
          : NavigatePage().NavigateToLogin(context);
    });
  }
}
