import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../Model/todo_model.dart';
import '../../view_models/todoViewModel.dart';
import '../../view_models/todo_addService.dart';

class AddTodoDetails extends StatefulWidget {
  const AddTodoDetails({super.key});

  @override
  State<AddTodoDetails> createState() => _AddTodoDetailsState();
}

class _AddTodoDetailsState extends State<AddTodoDetails> {
  TextEditingController text_title = TextEditingController();
  TextEditingController text_description = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final _auth = FirebaseAuth.instance;
  final service = todoAdd();
  @override
  Widget build(BuildContext context) {
    return Consumer<TodoViewModel>(
      builder: (BuildContext context, tododetailsProvider, Widget? child) =>
          Scaffold(
        appBar: AppBar(
          title: const Text("Add Task"),
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [
                  Colors.green.withOpacity(0.2),
                  Colors.blue.withOpacity(0.3),
                ])),
          ),
        ),
        body: Container(
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                Colors.green.withOpacity(0.2),
                Colors.blue.withOpacity(0.3),
              ])),
          child: Center(
            child: Column(
                // crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    "Add Your Task",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(
                    height: 27,
                  ),
                  Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          TextFormField(
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'required field';
                              }
                              return null;
                            },
                            controller: text_title,
                            decoration: const InputDecoration(
                                enabledBorder: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.white)),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.grey)),
                                fillColor: Color.fromARGB(255, 207, 206, 206),
                                hintText: 'Title',
                                prefixIcon: Icon(Icons.title_sharp)
                                // prefix: const Icon(Icons.email),
                                ),
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          TextFormField(
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'required field';
                              }
                              return null;
                            },
                            controller: text_description,
                            decoration: const InputDecoration(
                                enabledBorder: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.white)),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.grey)),
                                fillColor: Color.fromARGB(255, 207, 206, 206),
                                hintText: 'Description',
                                prefixIcon: Icon(Icons.description)
                                // prefix: const Icon(Icons.email),
                                ),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                          GestureDetector(
                            onTap: () {
                              if (_formKey.currentState!.validate()) {
                                StoreAllDetails(tododetailsProvider);
                              }
                            },
                            child: Container(
                              height: 50,
                              width: MediaQuery.of(context).size.width - 100,
                              decoration: BoxDecoration(
                                  color: Colors.black,
                                  borderRadius: BorderRadius.circular(10)),
                              child: const Padding(
                                padding: EdgeInsets.all(8.0),
                                child: Center(
                                  child: Text(
                                    "Add Task",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ))
                ]),
          ),
        ),
      ),
    );
  }

  StoreAllDetails(tododetailsProvider) async {
    // FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
    User? user = _auth.currentUser;

    Todo_model todoModel = Todo_model();
    var datetime = DateTime.now();
    todoModel.uid = user!.uid;
    todoModel.title = text_title.text;
    todoModel.description = text_description.text;
    todoModel.isCompleted = true;
    todoModel.issharewidth = [];
    todoModel.dateTime = datetime;

    todoModel.shareby = '';
    // todoModel.createdme = true;

    //todoModel.shareby = tododetailsProvider.login_usrname;

    service.StoreAllDetailsToFireStore(todoModel, context, datetime);
  }
}
