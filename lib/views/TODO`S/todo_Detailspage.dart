import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:systemtest/Router/navigateRouter.dart';

import '../../view_models/todoViewModel.dart';
import 'package:bottom_sheet/bottom_sheet.dart';

class TodoScreen extends StatefulWidget {
  const TodoScreen({super.key});

  @override
  State<TodoScreen> createState() => _TodoScreenState();
}

class _TodoScreenState extends State<TodoScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // getTodoDetails();
  }

  getTodoDetails() {
    Provider.of<TodoViewModel>(context, listen: false).GetTodoDetails();
  }

  TextEditingController text_title = TextEditingController();
  @override
  Widget build(BuildContext context) {
    // Provider.of<TodoViewModel>(context).GetTodoDetails();
    return Consumer<TodoViewModel>(
        builder: (BuildContext context, todoData, child) => StreamBuilder(
              stream: todoData.GetTodoDetails(),
              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                // if (snapshot.connectionState == ConnectionState.waiting) {
                //   return const Center(
                //     child: CircularProgressIndicator(),
                //   );
                // } else
                if (snapshot.hasError) {
                  return Center(
                    child: Text('Error: ${snapshot.error}'),
                  );
                } else {
                  final data = snapshot.data;
                  // Display the data however you want
                  return Scaffold(
                    backgroundColor: Colors.blue[50],
                    appBar: AppBar(
                      flexibleSpace: Container(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                                colors: [
                              Colors.green.withOpacity(0.2),
                              Colors.blue.withOpacity(0.3),
                            ])),
                      ),
                      title: Text(
                        "welcome- ${todoData.login_usrname}",
                        style: const TextStyle(fontWeight: FontWeight.bold),
                      ),
                      actions: [
                        IconButton(
                            onPressed: () {
                              todoData.logout(context);
                            },
                            icon: const Icon(Icons.logout))
                      ],
                    ),
                    body: !todoData.isloading
                        ? todoData.todosList.isNotEmpty
                            ? Container(
                                child: ListView.builder(
                                    itemCount: todoData.todosList.length,
                                    itemBuilder: ((context, index) {
                                      return TodoUi(todoData.todosList[index],
                                          todoData.login_usrname, todoData);
                                    })))
                            : const Center(
                                child: Text(
                                  "Your Todo List Is Empty, Please Add",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              )
                        : const Center(
                            child: CircularProgressIndicator.adaptive(),
                          ),
                    floatingActionButton: FloatingActionButton.extended(
                        backgroundColor: Colors.green,
                        onPressed: () {
                          NavigatePage().NavigateToTodoAddPage(context);
                        },
                        label: const Text("Add Data")),
                  );
                }
              },
            ));
  }

  Widget TodoUi(todovalues, useremail, alltodo) {
    var shareBy = todovalues.shareby;
    return Card(
      child: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.bottomRight,
                colors: [
              Colors.green.withOpacity(.3),
              Colors.blue.withOpacity(.5)
            ])),
        // height: 210,
        child: Column(
          children: [
            ListTile(
              leading: const Icon(Icons.today_outlined),
              title: Text(
                '${todovalues.title}',
                style: const TextStyle(overflow: TextOverflow.ellipsis),
              ),
              subtitle: Text(todovalues.description),
              trailing: Text('${todovalues.dateTime}'),
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                GestureDetector(
                  onTap: () {
                    showFlexibleBottomSheet(
                      minHeight: 0,
                      initHeight: 0.5,
                      maxHeight: 1,
                      context: context,
                      builder: ((context, scrollController, bottomSheetOffset) {
                        return _buildBottomSheet(context, scrollController,
                            bottomSheetOffset, alltodo, todovalues.dateTime);
                      }),
                      anchors: [0, 0.5, 1],
                      isSafeArea: true,
                    );

                    // alltodo.updateTodoDetailsById(todovalues.dateTime);
                    // updateValues(todovalues.dateTime);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.circular(5)),
                    child: const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text(
                        'Update',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ),
                if (todovalues.shareby != useremail)
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.circular(5)),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: shareBy != ''
                          ? Text('Share By :$shareBy ',
                              style: const TextStyle(
                                  color: Colors.white, fontSize: 12))
                          : const Icon(Icons.cancel),
                    ),
                  )
              ],
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: Container(
                child: IconButton(
                    onPressed: () {
                      print(todovalues.dateTime);
                      NavigatePage().NavigateToTaskShareScreen(
                          context,
                          '${todovalues.dateTime}',
                          '${todovalues.title}',
                          '${todovalues.description}');
                    },
                    icon: const Icon(Icons.share)),
              ),
            ),
            if (todovalues.shareby == useremail)
              Container(
                child: Text(
                  " Share With :${todovalues.issharewidth}",
                  style: const TextStyle(
                      overflow: TextOverflow.ellipsis, color: Colors.red),
                ),
              )
          ],
        ),
      ),
    );
  }

  updateValues(datetime) {}

  Widget _buildBottomSheet(
      BuildContext context,
      ScrollController scrollController,
      double bottomSheetOffset,
      alltodo,
      datetime) {
    return Material(
      child: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
              Colors.green.withOpacity(0.2),
              Colors.blue.withOpacity(0.3),
            ])),
        padding: const EdgeInsets.all(20),
        child: ListView(
          controller: scrollController,
          children: [
            Align(
              alignment: Alignment.topRight,
              child: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: const Icon(Icons.cancel)),
            ),
            const Divider(),
            const SizedBox(
              height: 10,
            ),
            TextFormField(
              validator: (value) {
                if (value!.isEmpty) {
                  return 'required field';
                }
                return null;
              },
              controller: text_title,
              decoration: const InputDecoration(
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.white)),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey)),
                  fillColor: Color.fromARGB(255, 207, 206, 206),
                  hintText: 'Update Description',
                  prefixIcon: Icon(Icons.description)
                  // prefix: const Icon(Icons.email),
                  ),
            ),
            const SizedBox(
              height: 25,
            ),
            GestureDetector(
              onTap: () {
                if (text_title.toString().length > 5) {
                  alltodo.updateTodoDetailsById(datetime, text_title.text);
                  Navigator.pop(context);
                } else {
                  Navigator.pop(context);
                  Future.delayed(const Duration(seconds: 2), () {
                    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                        backgroundColor: Colors.red,
                        content: Text(
                            "Your Description is to Short, please update with valid description")));
                  });
                }
              },
              child: Container(
                height: 50,
                width: MediaQuery.of(context).size.width - 180,
                decoration: BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.circular(10)),
                child: const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Center(
                    child: Text(
                      "Update",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
