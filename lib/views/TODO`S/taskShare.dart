import 'package:flutter/material.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:provider/provider.dart';
import 'package:systemtest/view_models/todo_addService.dart';

import '../../Model/todo_model.dart';
import '../../view_models/todoViewModel.dart';

class Todo_TaskShare extends StatefulWidget {
  String? dateTime;
  String? title;
  String? description;
  Todo_TaskShare(
      {super.key,
      required this.dateTime,
      required this.title,
      required this.description}) {
    dateTime = dateTime;
    title = title;
    description = description;
  }

  @override
  State<Todo_TaskShare> createState() =>
      _Todo_TaskShareState(dateTime, title, description);
}

class _Todo_TaskShareState extends State<Todo_TaskShare> {
  String? dateTime;
  String? title;
  String? description;
  _Todo_TaskShareState(String? dateTime, String? title, String? description) {
    this.dateTime = dateTime;
    this.title = title;
    this.description = description;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getAllRegisteduser();
  }

  List userDetails = [];
  var selectedValue;
  getAllRegisteduser() {
    todoAdd().getAllRegisteredEmail().then((value) {
      setState(() {
        userDetails = value;

        print("Share detais");
        print(userDetails);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<TodoViewModel>(
      builder: (BuildContext context, todoData, Widget? child) => Scaffold(
        appBar: AppBar(
          title: Text("Share Task $dateTime"),
        ),
        body: Container(
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                Container(
                  child: Column(children: [
                    const SizedBox(
                      height: 20,
                    ),
                    const Text("Share By Registered Email"),
                    const SizedBox(
                      height: 16,
                    ),
                    Container(
                      decoration: const BoxDecoration(
                          color: Color.fromARGB(255, 197, 206, 246)),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton2(
                          isExpanded: true,
                          hint: const Row(
                            children: [
                              Icon(
                                Icons.list,
                                size: 16,
                                color: Colors.red,
                              ),
                              SizedBox(
                                width: 4,
                              ),
                              Expanded(
                                child: Text(
                                  'Select Email To Share',
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ],
                          ),
                          items: userDetails
                              .map((item) => DropdownMenuItem(
                                    value: item.email,
                                    child: Text(
                                      item.email,
                                      style: const TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                      ),
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ))
                              .toList(),
                          value: selectedValue,
                          onChanged: (value) {
                            setState(() {
                              selectedValue = value;
                            });
                          },
                          menuItemStyleData: const MenuItemStyleData(
                            height: 40,
                            padding: EdgeInsets.only(left: 14, right: 14),
                          ),
                        ),
                      ),
                    ),
                  ]),
                ),
                const SizedBox(
                  height: 40,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width - 100,
                  child: ElevatedButton.icon(
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.black),
                      onPressed: () {
                        if (selectedValue == null) {
                          ScaffoldMessenger.of(context).showSnackBar(
                              const SnackBar(
                                  backgroundColor: Colors.red,
                                  content: Text("Please Select Email First")));
                        } else if (selectedValue == todoData.login_usrname) {
                          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                              backgroundColor: Colors.red,
                              content: Text(
                                  "Please Select Another Registered Email, this is your Email")));
                        } else {
                          Todo_model todoModel = Todo_model();

                          todoModel.issharewidth = [selectedValue];
                          todoAdd().ShareTodoToAnotherUSer(
                              selectedValue,
                              todoData.login_usrname,
                              context,
                              dateTime,
                              title,
                              description);
                        }
                      },
                      icon: const Icon(Icons.share),
                      label: const Text(
                        "share",
                        style: TextStyle(color: Colors.white),
                      )),
                )
              ],
            )),
      ),
    );
  }
}
